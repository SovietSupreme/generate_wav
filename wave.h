/* Written By joseph sloan joe.sloan@outlook.com 2019 */

#include <stdio.h>
#include <math.h>

#if defined(__MACH__)
#include <stdlib.h>
#else 
#include <malloc.h>
#endif

typedef enum bitdepth {
	B8 	= 8,
	B16 = 16,
	B32 = 32
} BitDepth;

typedef struct WaveHeader {
    // Riff Wave Header
    char chunkId[4];
    int  chunkSize;
    char format[4];

    // Format Subchunk
    char subChunk1Id[4];
    int  subChunk1Size;
    short int audioFormat;
    short int numChannels;
    int sampleRate;
    int byteRate;
    short int blockAlign;
    short int bitsPerSample;
    //short int extraParamSize;

    // Data Subchunk
    char subChunk2Id[4];
    int  subChunk2Size;

} WaveHeader;

typedef struct Wave {
    WaveHeader header;
    char* data;
    long long int index;
    long long int size;
    long long int nSamples;
} Wave;

void waveCreateSine(float sampleRate, float frequency, BitDepth depth, float phaseOffset,int cycles);
void waveCreateSquare(float sampleRate, float frequency, BitDepth depth, float phaseOffset, int cycles);
void waveCreateSaw(float sampleRate, float frequency, BitDepth depth, float phaseOffset, int cycles);
void waveCreateTri(float sampleRate, float frequency, BitDepth depth, float phaseOffset, int cycles);

