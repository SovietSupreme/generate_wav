#include <stdio.h>
#include "wave.h"

int main(int argc,char** argv){
    float sampleRate = 48000.00;
	float frequency = 440.00;
	int depth = 16;
	float phaseOffset = 0.00;
	int cycles = 1;

	printf("Enter Desired Sample Rate: ");
	scanf("%f",&sampleRate);
	printf("Enter Desired Frequency: ");
	scanf("%f",&frequency);
	printf("Enter Desired Bit Depth: ");
	scanf("%d",&depth);
	printf("Enter Desired Phase Offset (%f for example): ",M_PI);
	scanf("%f",&phaseOffset);
	printf("Enter Desired Number of Cycles: ");
	scanf("%d",&cycles);


    waveCreateSine(sampleRate,frequency,(BitDepth)depth,phaseOffset,cycles);
    waveCreateSquare(sampleRate,frequency,(BitDepth)depth,phaseOffset,cycles);
	waveCreateSaw(sampleRate,frequency,(BitDepth)depth,phaseOffset,cycles);
	waveCreateTri(sampleRate,frequency,(BitDepth)depth,phaseOffset,cycles);

    return 0;
}

